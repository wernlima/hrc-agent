from functions import get_names_of_available_objects, get_pose_from_object
from langchain.agents import tool
from pymongo import MongoClient
import time

from pymongo import MongoClient
from datetime import datetime

# Connect to MongoDB
client = MongoClient('localhost', 27017)
db = client['environment_db']

# Get the current date to use in the collection name
formatted_date = datetime.now().strftime("%Y-%m-%d")
collection_name = f"Log_{formatted_date}"
log_collection = db[collection_name]

def log_command_out(command):
    log_entry = {
        "type": "out",
        "timestamp": datetime.now(),
        "command": command
    }
    log_collection.insert_one(log_entry)

def log_command_in(command):
    log_entry = {
        "type": "in",
        "timestamp": datetime.now(),
        "command": command
    }
    log_collection.insert_one(log_entry)

@tool
def pick_place(object_name, goal_pose):
    """Performs the pick and place of an object to the goal pose and returns True."""
    log_command_out(f"pick_place called with object_name: {object_name}, goal_pose: {goal_pose}")
    print(f"We perform the pick and place of {object_name} to {goal_pose}")
    time.sleep(2)
    return_value=True
    log_command_in(f"{return_value}")
    return return_value

@tool
def go_to_ee_pos(position, orientation):
    """Performs the end effector movement to a specified position with orientation and returns True. Standard orientation is 0,0,1,0)"""
    log_command_out(f"go_to_ee_pos called with position: {position}, orientation: {orientation}")
    print(f"We perform the ee movement to pos {position} with orientation {orientation}")
    time.sleep(2)
    return_value = f"We have reached the target {position}!"
    log_command_in(f"{return_value} Position error of the EE is {0} meters")
    return return_value

@tool
def start_full_compliancy():
    """Starts full compliancy and returns True."""
    log_command_out("start_full_compliancy called")
    print("Full Compliancy started")
    time.sleep(2)
    return_value=True
    log_command_in(f"{return_value}")
    return return_value

@tool
def stop_full_compliancy():
    """Ends full compliancy and returns True."""
    log_command_out("stop_full_compliancy called")
    print("Full Compliancy ended")
    time.sleep(2)
    return_value=True
    log_command_in(f"{return_value}")
    return return_value

@tool
def gripper_open():
    """Opens the gripper and returns True."""
    log_command_out("gripper_open called")
    print("gripper opened")
    time.sleep(2)
    return_value=True
    log_command_in(f"{return_value}")
    return return_value

@tool
def gripper_close():
    """Closes the gripper and returns True."""
    log_command_out("gripper_close called")
    print("gripper closed")
    time.sleep(2)
    return_value=True
    log_command_in(f"{return_value}")
    return return_value