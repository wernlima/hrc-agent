import subprocess
import time
import rclpy
from rclpy.node import Node
from std_srvs.srv import Trigger
from std_msgs.msg import String
from geometry_msgs.msg import Pose, PoseArray


###################################################################################
################################# Pick and Place ##################################
###################################################################################

class PickPlacePublisher(Node):
    def __init__(self, object_name, goal_pose):
        super().__init__('pick_place_publisher')
        self.publisher_ = self.create_publisher(String, '/pick_place_cube', 10)
        self.object_name = object_name
        self.goal_pose = goal_pose
        self.publish_message()

    def publish_message(self):
        msg = String()
        msg.data = '{{"cube_name": "{}", "goal_pose": {}}}'.format(self.object_name, self.goal_pose)
        self.publisher_.publish(msg)
        self.get_logger().info('Published: "%s"' % msg.data)

def pick_place(object_name, goal_pose):
    rclpy.init()
    pick_place_publisher = PickPlacePublisher(object_name, goal_pose)
    
    # Let the message be published
    rclpy.spin_once(pick_place_publisher, timeout_sec=1.0)
    
    # Clean up
    pick_place_publisher.destroy_node()
    rclpy.shutdown()

###################################################################################
################################## Go to EE Pos ###################################
###################################################################################

class GoToEEPosPublisher(Node):
    def __init__(self, position, orientation):
        super().__init__('go_to_ee_pos_publisher')
        self.publisher_ = self.create_publisher(PoseArray, '/go_to_ee_pos', 10)
        self.position = position
        self.orientation = orientation
        self.publish_message()

    def publish_message(self):
        pose_array = PoseArray()
        pose = Pose()
        pose.position.x = float(self.position[0])
        pose.position.y = float(self.position[1])
        pose.position.z = float(self.position[2])
        pose.orientation.x = float(self.orientation[0])
        pose.orientation.y = float(self.orientation[1])
        pose.orientation.z = float(self.orientation[2])
        pose.orientation.w = float(self.orientation[3])
        pose_array.poses.append(pose)
        self.publisher_.publish(pose_array)
        self.get_logger().info('Published position: "{}" and orientation: "{}"'.format(self.position, self.orientation))

def go_to_ee_pos(position, orientation):
    rclpy.init()
    go_to_ee_pos_publisher = GoToEEPosPublisher(position, orientation)
    
    # Let the message be published
    rclpy.spin_once(go_to_ee_pos_publisher, timeout_sec=1.0)
    
    # Clean up
    go_to_ee_pos_publisher.destroy_node()
    rclpy.shutdown()


###################################################################################
################################ Start Compliancy #################################
###################################################################################

def start_full_compliancy():
    # Initialize the ROS 2 Python client library
    rclpy.init()

    # Create a Node
    node = Node('compliance_service_caller')

    # Create a client for the 'relax' service
    client = node.create_client(Trigger, '/compliant_trajectory_controller/relax')

    # Wait for the service to be available
    while not client.wait_for_service(timeout_sec=1.0):
        node.get_logger().info('Service not available, waiting...')

    # Create a request
    request = Trigger.Request()

    # Call the service
    future = client.call_async(request)

    # Wait for the result
    rclpy.spin_until_future_complete(node, future)

    if future.result() is not None:
        response = future.result()
        node.get_logger().info(f'Result: success={response.success}, message="{response.message}"')
    else:
        node.get_logger().error('Service call failed')

    # Shutdown the ROS 2 Python client library
    rclpy.shutdown()


###################################################################################
################################# Stop Compliancy #################################
###################################################################################

def stop_full_compliancy():
    # Initialize the ROS 2 Python client library
    rclpy.init()

    # Create a Node
    node = Node('compliance_service_caller')

    # Create a client for the 'tighten_up' service
    client = node.create_client(Trigger, '/compliant_trajectory_controller/tighten_up')

    # Wait for the service to be available
    while not client.wait_for_service(timeout_sec=1.0):
        node.get_logger().info('Service not available, waiting...')

    # Create a request
    # Create a request
    request = Trigger.Request()

    # Call the service
    future = client.call_async(request)

    # Wait for the result
    rclpy.spin_until_future_complete(node, future)

    if future.result() is not None:
        response = future.result()
        node.get_logger().info(f'Result: success={response.success}, message="{response.message}"')
    else:
        node.get_logger().error('Service call failed')

    # Shutdown the ROS 2 Python client library
    rclpy.shutdown()

###################################################################################
################################## Open Gripper ###################################
###################################################################################

def gripper_open():
    command = [
        'ros2', 'action', 'send_goal', 
        '/panda_gripper/gripper_action', 
        'control_msgs/action/GripperCommand', 
        '{command: {position: 0.039, max_effort: 40.0}}'
    ]
    result = subprocess.run(command, capture_output=True, text=True)
    if result.returncode == 0:
        print("Gripper opened successfully")
        print("Output:", result.stdout)
    else:
        print("Failed to open gripper")
        print("Error:", result.stderr)


###################################################################################
################################# Close Gripper ###################################
###################################################################################

def gripper_close():
    command = [
        'ros2', 'action', 'send_goal', 
        '/panda_gripper/gripper_action', 
        'control_msgs/action/GripperCommand', 
        '{command: {position: 0.01, max_effort: 40.0}}'
    ]
    result = subprocess.run(command, capture_output=True, text=True)
    if result.returncode == 0:
        print("Gripper closed successfully")
        print("Output:", result.stdout)
    else:
        print("Failed to close gripper")
        print("Error:", result.stderr)


###################################################################################
