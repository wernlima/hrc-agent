# Import necessary libraries and modules
from functions_sim import (
    pick_place, go_to_ee_pos, start_full_compliancy, stop_full_compliancy, 
    gripper_open, gripper_close, get_names_of_available_objects, get_pose_from_object
)
from langchain_openai import ChatOpenAI
from credentials import OpenAI_API_KEY
from langchain_google_vertexai import ChatVertexAI
import vertexai
from langchain_core.prompts import ChatPromptTemplate, MessagesPlaceholder
from langchain.agents.format_scratchpad.openai_tools import format_to_openai_tool_messages
from langchain.agents.output_parsers.openai_tools import OpenAIToolsAgentOutputParser
from langchain.agents import AgentExecutor
from gtts import gTTS
from langchain_core.messages import AIMessage, HumanMessage
import pygame
import time
import speech_recognition as sr
from datetime import datetime

from pymongo import MongoClient
from datetime import datetime

# Connect to MongoDB
client = MongoClient('localhost', 27017)
db = client['environment_db']

# Get the current date to use in the collection name
formatted_date = datetime.now().strftime("%Y-%m-%d")
collection_name = f"Log_{formatted_date}"
log_collection = db[collection_name]

def log_command_out(command):
    log_entry = {
        "type": "agent_out",
        "timestamp": datetime.now(),
        "command": command
    }
    log_collection.insert_one(log_entry)

def log_command_in(command):
    log_entry = {
        "type": "agent_in",
        "timestamp": datetime.now(),
        "command": command
    }
    log_collection.insert_one(log_entry)


def log_command_sys(command):
    log_entry = {
        "type": "start",
        "timestamp": datetime.now(),
        "command": command
    }
    log_collection.insert_one(log_entry)

log_command_sys("-----------------------\n New conversation started")

TEXT_INPUT = 1
TTS_WITH_OPENAI = 0
AUDIO_OUTPUT=0

# Initialize pygame mixer
pygame.mixer.init()

# Initialize the GPT models
GPT_4o_mini = ChatOpenAI(model="gpt-4o-mini", temperature=0.01, openai_api_key=OpenAI_API_KEY)    

# Define the LLM
llm = GPT_4o_mini

# Define the tools
tools = [gripper_open, gripper_close, go_to_ee_pos, start_full_compliancy, stop_full_compliancy, get_names_of_available_objects, get_pose_from_object]

# Define the prompt
prompt = ChatPromptTemplate.from_messages(
    [
        ("system", "You are a very powerful assistant, controlling a robot arm. go_to_ee_pos has the format for the home position of ('position': [0.3, -0.0, 0.5], 'orientation': [0, 0, 1, 0]). With the robotic arm, you can interact with the available objects, also you have a history. Make sure z is always larger than 0.1, but also low enough to facilitate successful interactions with objects. Only change the z-Coordinate if it is strictly below 0.1"),
        ("placeholder", "{chat_history}"),
        ("user", "{input}"),
        MessagesPlaceholder(variable_name="agent_scratchpad"),
    ]   
)

# Bind tools to the LLM
llm_with_tools = llm.bind_tools(tools)

# Define the agent
agent = (
    {
        "input": lambda x: x["input"],
        "chat_history":  lambda x: x["chat_history"],
        "agent_scratchpad": lambda x: format_to_openai_tool_messages(x["intermediate_steps"]),
    }
    | prompt
    | llm_with_tools
    | OpenAIToolsAgentOutputParser()
)

# Create the AgentExecutor
agent_executor = AgentExecutor(agent=agent, tools=tools, verbose=True)

# Initialize interaction history
history = []
history_limit = 5
history_prompt = []

# Initialize command log file
formatted_date = datetime.now().strftime("%Y-%m-%d")
command_log_file = f"log/agent_log_{formatted_date}.txt"

def listen():
    # Use the default microphone as the audio source
    with sr.Microphone() as source:
        print("Adjusting for ambient noise... Please wait.")
        # Adjust for ambient noise levels
        r.adjust_for_ambient_noise(source)
        print("Listening...")
        
        try:
            # Listen to the microphone input
            audio_listened = r.listen(source)
            print("Processing...")
            
            ## Start processing time
            start_time = time.time()
            
            # Using the Whisper model for speech recognition
            """text = r.recognize_whisper(
                audio_listened,
                model="large-v3",
                language="english".9*3++     
            )"""
            text = r.recognize_whisper(
                audio_listened,
                model="tiny.en",
                language="english"
            )
            ## End processing time
            end_time = time.time()
            
            
            # Calculate and print the processing time
            processing_time = end_time - start_time
            print("-"*30)
            print(f"Processing time: {processing_time:.2f} seconds")
            print(f"processed text: {text}")
            print("-"*30)
            return text

        except sr.UnknownValueError:
            print("Speech Recognition could not understand audio")
            return None

        except sr.RequestError as e:
            print(f"Could not request results from Speech Recognition service; {e}")
            return None

        except Exception as e:
            print(f"Error: {e}")
            return None


# Continuous loop for interaction
while True:
    if TEXT_INPUT:
        user_input = input("Enter your command: ")
    else:
        r = sr.Recognizer()
        user_input = listen()

    if user_input.lower() in ["exit", "quit", "end", "fuck off", "exit.", "quit.", "end."]:
        print("Exiting the interaction.")
        break
    # Log and add user input to history

    log_command_in(f"User Command: {user_input}")
    history.append(HumanMessage(content=user_input))
    
    # Ensure history does not exceed the limit
    if len(history) > history_limit:
        history.pop(0)
    
    # Prepare the history for the agent
    history_for_agent = [{"role": "user", "content": entry.content} if isinstance(entry, HumanMessage) else {"role": "assistant", "content": entry.content} for entry in history]
    
    # Get the response from the agent
    response = agent_executor.invoke(
    {
        "input": user_input,
        "chat_history": history_for_agent,
    }
)
    response_text = response['output']

    # Log the command executed by the agent
    log_command_out(f"Agent Response: {response_text}")
    
    # Add agent response to history
    history.append(AIMessage(content=response_text))
    
    # Ensure history does not exceed the limit
    if len(history) > history_limit:
        history.pop(0)
    
    # Print response
    print("-" * 30)
    print(response_text)
    if AUDIO_OUTPUT or TTS_WITH_OPENAI:
        if TTS_WITH_OPENAI:
            from openai import OpenAI
            import pygame
            from credentials import OpenAI_API_KEY
            client = OpenAI(api_key=OpenAI_API_KEY)

            response = client.audio.speech.create(
                model="tts-1",
                voice="shimmer",
                input=response_text,
            )

            response.write_to_file("output.mp3")

            pygame.mixer.init()
            pygame.mixer.music.load("output.mp3")
            pygame.mixer.music.play()
            while pygame.mixer.music.get_busy():
                pygame.time.Clock().tick(10)

        else:
        # Text to speech
            tts = gTTS(text=response_text, lang='en')
            tts.save("output.mp3")
            pygame.mixer.music.load("output.mp3")
            pygame.mixer.music.play()
            while pygame.mixer.music.get_busy():
                pygame.time.Clock().tick(10)
            print("-" * 30)
