from pynput import keyboard
import subprocess

muted = False
 
def toggle_mute():
    global muted
    try:
        if muted:
            subprocess.run(['pactl', 'set-source-mute', '@DEFAULT_SOURCE@', '0'], check=True)
            #print("Microphone Unmuted")
        else:
            subprocess.run(['pactl', 'set-source-mute', '@DEFAULT_SOURCE@', '1'], check=True)
            #print("Microphone Muted")
        muted = not muted
    except subprocess.CalledProcessError as e:
        print("Error toggling microphone:", e)
  
def on_press(key):
    if key == keyboard.Key.space:
        toggle_mute()

with keyboard.Listener(on_press=on_press) as listener:
    listener.join()
    