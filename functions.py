import subprocess
import rclpy
from rclpy.node import Node
import numpy as np
from std_srvs.srv import Trigger
from std_msgs.msg import String
from geometry_msgs.msg import Pose, PoseArray, PoseStamped
from pymongo import MongoClient
from langchain.agents import tool
import time
from datetime import datetime



# Initialize command log file
from pymongo import MongoClient
from datetime import datetime

# Connect to MongoDB
client = MongoClient('localhost', 27017)
db = client['environment_db']

# Get the current date to use in the collection name
formatted_date = datetime.now().strftime("%Y-%m-%d")
collection_name = f"Log_{formatted_date}"
log_collection = db[collection_name]

def log_command_out(command):
    log_entry = {
        "type": "out",
        "timestamp": datetime.now(),
        "command": command
    }
    log_collection.insert_one(log_entry)

def log_command_in(command):
    log_entry = {
        "type": "in",
        "timestamp": datetime.now(),
        "command": command
    }
    log_collection.insert_one(log_entry)


###################################################################################
################################# Pick and Place ##################################
###################################################################################

class PickPlacePublisher(Node):
    def __init__(self, object_name, goal_pose):
        super().__init__('pick_place_publisher')
        self.publisher_ = self.create_publisher(String, '/pick_place_cube', 10)
        self.object_name = object_name
        self.goal_pose = goal_pose
        self.publish_message()

    def publish_message(self):
        msg = String()
        msg.data = '{{"cube_name": "{}", "goal_pose": {}}}'.format(self.object_name, self.goal_pose)
        self.publisher_.publish(msg)
        self.get_logger().info('Published: "%s"' % msg.data)
@tool
def pick_place(object_name, goal_pose):
    """Performs the pick and place of an object to the goal pose and returns True."""
    log_command_out(f"pick_place called with object_name: {object_name}, goal_pose: {goal_pose}")
    rclpy.init()
    pick_place_publisher = PickPlacePublisher(object_name, goal_pose)
    rclpy.spin_once(pick_place_publisher, timeout_sec=1.0)
    pick_place_publisher.destroy_node()
    rclpy.shutdown()
    result = True
    log_command_in(result)
    return result

###################################################################################
################################## Go to EE Pos ###################################
###################################################################################

class GoToEEPosPublisher(Node):
    def __init__(self, position, orientation):
        super().__init__('go_to_ee_pos_publisher')
        self.publisher_ = self.create_publisher(PoseArray, '/go_to_ee_pos', 10)
        self.position = position
        self.orientation = orientation
        self.publish_message()

    def publish_message(self):
        pose_array = PoseArray()
        pose = Pose()
        pose.position.x = float(self.position[0])
        pose.position.y = float(self.position[1])
        pose.position.z = float(self.position[2])
        pose.orientation.x = float(self.orientation[0])
        pose.orientation.y = float(self.orientation[1])
        pose.orientation.z = float(self.orientation[2])
        pose.orientation.w = float(self.orientation[3])
        pose_array.poses.append(pose)
        self.publisher_.publish(pose_array)
        self.get_logger().info('Published position: "{}" and orientation: "{}"'.format(self.position, self.orientation))


class ActualEEPositionSubscriber(Node):
    def __init__(self):
        super().__init__('actual_ee_position_subscriber')
        self.received = False
        self.current_position = None
        
        # Subscription to the current pose topic
        self.subscription = self.create_subscription(
            PoseStamped,
            '/franka_robot_state_broadcaster/current_pose',
            self.listener_callback,
            10)
        
    def listener_callback(self, msg):
        # Update current position and set received flag
        self.current_position = [msg.pose.position.x, msg.pose.position.y, msg.pose.position.z]
        self.received = True

def get_actual_ee_position():
    """Fetch the current end-effector position."""
    ee_position_subscriber = ActualEEPositionSubscriber()

    # Spin the node to ensure we receive the message
    while not ee_position_subscriber.received:
        rclpy.spin_once(ee_position_subscriber, timeout_sec=0.1)
    
    # Extract the position once received
    actual_position = ee_position_subscriber.current_position
    
    # Clean up and shutdown the node
    ee_position_subscriber.destroy_node()

    return actual_position

@tool
def go_to_ee_pos(position, orientation):
    """Performs the end effector movement to a specified position with orientation and returns a message indicating the position error."""
    log_command_out(f"go_to_ee_pos called with position: {position}, orientation: {orientation}")
    
    # Initialize ROS2
    rclpy.init()
    go_to_ee_pos_publisher = GoToEEPosPublisher(position, orientation)
    
    start_time = time.time()
    threshold = 0.15
    timeout = 10
    
    error = float('inf')
    actual_ee_position = None
    
    while time.time() - start_time < timeout:
        # Get the actual end-effector position
        actual_ee_position = get_actual_ee_position()
        
        # Calculate the position error
        error = np.linalg.norm(np.array(position) - np.array(actual_ee_position))
        
        # Check if the error is within the acceptable threshold
        print(f"{error}")
        if error < threshold:
            break
        
        # Short sleep to prevent overloading the loop
        time.sleep(0.1)
    
    # If the timeout is reached, we return the current position and error
    if error >= threshold:
        return_value = f"Timeout reached. Current EE position: {actual_ee_position}, position error: {error:.4f} meters"
    else:
        return_value = f"We have reached the target {position}!"
    
    # Clean up
    go_to_ee_pos_publisher.destroy_node()
    rclpy.shutdown()
    
    log_command_in(f"{return_value} Position error of the EE is {error:.4f} meters")
    return return_value
###################################################################################
################################ Start Compliancy #################################
###################################################################################
@tool
def start_full_compliancy():
    """Starts full compliancy and returns True."""
    log_command_out("start_full_compliancy called")
    # Initialize the ROS 2 Python client library
    rclpy.init()

    # Create a Node
    node = Node('compliance_service_caller')

    # Create a client for the 'relax' service
    client = node.create_client(Trigger, '/compliant_trajectory_controller/relax')

    # Wait for the service to be available
    while not client.wait_for_service(timeout_sec=1.0):
        node.get_logger().info('Service not available, waiting...')

    # Create a request
    request = Trigger.Request()

    # Call the service
    future = client.call_async(request)

    # Wait for the result
    rclpy.spin_until_future_complete(node, future)

    if future.result() is not None:
        response = future.result()
        node.get_logger().info(f'Result: success={response.success}, message="{response.message}"')
    else:
        node.get_logger().error('Service call failed')

    # Shutdown the ROS 2 Python client library
    rclpy.shutdown()
    return_value=True
    log_command_in(f"{return_value}")
    return return_value

###################################################################################
################################# Stop Compliancy #################################
###################################################################################
@tool
def stop_full_compliancy():
    """Ends full compliancy and returns True."""
    log_command_out("stop_full_compliancy called")
    # Initialize the ROS 2 Python client library
    rclpy.init()

    # Create a Node
    node = Node('compliance_service_caller')

    # Create a client for the 'tighten_up' service
    client = node.create_client(Trigger, '/compliant_trajectory_controller/tighten_up')

    # Wait for the service to be available
    while not client.wait_for_service(timeout_sec=1.0):
        node.get_logger().info('Service not available, waiting...')

    # Create a request
    # Create a request
    request = Trigger.Request()

    # Call the service
    future = client.call_async(request)

    # Wait for the result
    rclpy.spin_until_future_complete(node, future)

    if future.result() is not None:
        response = future.result()
        node.get_logger().info(f'Result: success={response.success}, message="{response.message}"')
    else:
        node.get_logger().error('Service call failed')

    # Shutdown the ROS 2 Python client library
    rclpy.shutdown()
    return_value=True
    log_command_in(f"{return_value}")
    return return_value
###################################################################################
################################## Open Gripper ###################################
###################################################################################
@tool
def gripper_open():
    """Opens the gripper and returns True."""
    log_command_out("gripper_open called")
    command = [
        'ros2', 'action', 'send_goal', 
        '/panda_gripper/gripper_action', 
        'control_msgs/action/GripperCommand', 
        '{command: {position: 0.039, max_effort: 40.0}}'
    ]
    result = subprocess.run(command, capture_output=True, text=True)
    if result.returncode == 0:
        print("Gripper opened successfully")
        print("Output:", result.stdout)
    else:
        print("Failed to open gripper")
        print("Error:", result.stderr)
    #time.sleep(5)
    return_value=True
    log_command_in(f"{return_value}")
    return return_value

###################################################################################
################################# Close Gripper ###################################
###################################################################################
@tool
def gripper_close():
    """Closes the gripper and returns True."""
    log_command_out("gripper_close called")
    command = [
        'ros2', 'action', 'send_goal', 
        '/panda_gripper/gripper_action', 
        'control_msgs/action/GripperCommand', 
        '{command: {position: 0.01, max_effort: 40.0}}'
    ]
    result = subprocess.run(command, capture_output=True, text=True)
    return_value=True
    log_command_in(f"{return_value}")
    return return_value
    if result.returncode == 0:
        print("Gripper closed successfully")
        print("Output:", result.stdout)
    else:
        print("Failed to close gripper")
        print("Error:", result.stderr)
    #time.sleep(5)
    return_value=True
    log_command_in(f"{return_value}")
    return return_value

###################################################################################
########################## Get Names of Available Objects #########################
###################################################################################
@tool
def get_names_of_available_objects():
    """Retrieve names of available objects from the database.Returns:list: A list of names of objects retrieved from the database."""
    log_command_out("get_names_of_available_objects called")
    client = MongoClient('localhost', 27017)
    db = client['environment_db']
    objects = list(db.objects.find({}))
    return_value=[obj['name'] for obj in objects]
    log_command_in(f"{return_value}")
    return return_value

###################################################################################
############################# Get Pose from Object ################################
###################################################################################
@tool
def get_pose_from_object(name):
    """Retrieve the pose information for an object with a given name.Args: name (str): The name of the object. Returns: list: The pose information of the object."""
    log_command_out(f"get_pose_from_object called with name: {name}")
    client = MongoClient('localhost', 27017)
    db = client['environment_db']
    obj = db.objects.find_one({'name': name})
    if obj:
        return_value = obj.get('pose', {}).get('position')
        log_command_in(f"{return_value}")
        return return_value
    log_command_in("None - obj is empty")
    return None

###################################################################################
