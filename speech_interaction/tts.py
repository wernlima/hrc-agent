from gtts import gTTS
import pygame

# The text you want to convert to speech
text = "Hi,I am going to convert text into speech for you!"

# Create a gTTS object
tts = gTTS(text=text, lang='en')

# Save the audio file
tts.save("output.mp3")

# Initialize pygame mixer
pygame.mixer.init()

# Load the mp3 file
pygame.mixer.music.load("output.mp3")

# Play the mp3 file
pygame.mixer.music.play()

# Keep the script running until the audio finishes
while pygame.mixer.music.get_busy():
    pygame.time.Clock().tick(10)
