import speech_recognition as sr
import time  # Import the time module

# Create an instance of the Recognizer class
r = sr.Recognizer()

def listen():
    # Use the default microphone as the audio source
     with sr.Microphone(device_index=6) as source:
        print("Adjusting for ambient noise... Please wait.")
        # Adjust for ambient noise levels
        r.adjust_for_ambient_noise(source)
        print("Listening...")
        
        try:
            # Listen to the microphone input
            audio_listened = r.listen(source)
            print("Processing...")
            
            ## Start processing time
            start_time = time.time()
            
            # Using the Whisper model for speech recognition
            """text = r.recognize_whisper(
                audio_listened,
                model="large-v3",
                language="english"
            )"""
            text = r.recognize_whisper(
                audio_listened,
                model="tiny.en",
                language="english"
            )
            ## End processing time
            end_time = time.time()
            
            # Calculate and print the processing time
            processing_time = end_time - start_time
            print(f"Processing time: {processing_time:.2f} seconds")

            return text

        except sr.UnknownValueError:
            print("Speech Recognition could not understand audio")
            return None

        except sr.RequestError as e:
            print(f"Could not request results from Speech Recognition service; {e}")
            return None

        except Exception as e:
            print(f"Error: {e}")
            return None

if __name__ == "__main__":
    # Call the listen function and print the final recognized text
    result = listen()
    if result:
        print("-"*30)
        print(f"Final recognized text: {result}")
        print("-"*30)
    else:
        print("No text recognized.")
