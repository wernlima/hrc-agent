import speech_recognition as sr
import time
import whisper
import torch
import numpy as np

# Create an instance of the Recognizer class
r = sr.Recognizer()

# Load the Whisper model and move it to the GPU if available
device = "cuda" if torch.cuda.is_available() else "cpu"
torch_dtype = torch.float32

model = whisper.load_model("large-v3").to(device)

def listen():
    # Use the default microphone as the audio source
    with sr.Microphone(device_index=11) as source:
        print("Adjusting for ambient noise... Please wait.")
        # Adjust for ambient noise levels
        r.adjust_for_ambient_noise(source)
        print("Listening...")
        
        try:
            # Listen to the microphone input
            audio_listened = r.listen(source)
            print("Processing...")

            ## Start processing time
            start_time = time.time()
            
            # Convert the audio to a NumPy array with dtype=float32
            audio_data = np.frombuffer(audio_listened.get_raw_data(), np.int16).astype(np.float32) / 32768.0
            # Resample if necessary
            audio_data = whisper.pad_or_trim(audio_data)
            mel = whisper.log_mel_spectrogram(audio_data).to(device)
            
            # Process the audio with the Whisper model
            result = model.decode(mel)
            
            ## End processing time
            end_time = time.time()
            
            # Calculate and print the processing time
            processing_time = end_time - start_time
            print(f"Processing time: {processing_time:.2f} seconds")

            return result["text"]

        except sr.UnknownValueError:
            print("Speech Recognition could not understand audio")
            return None

        except sr.RequestError as e:
            print(f"Could not request results from Speech Recognition service; {e}")
            return None

        except Exception as e:
            print(f"Error: {e}")
            return None

if __name__ == "__main__":
    # Call the listen function and print the final recognized text
    result = listen()
    if result:
        print("-"*30)
        print(f"Final recognized text: {result}")
        print("-"*30)
    else:
        print("No text recognized.")
