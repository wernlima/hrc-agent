from openai import OpenAI
import pygame
from credentials import OpenAI_API_KEY
client = OpenAI(api_key=OpenAI_API_KEY)

response = client.audio.speech.create(
    model="tts-1",
    voice="shimmer",
    input="Hello world! This is a streaming test.",
)

response.write_to_file("output.mp3")

pygame.mixer.init()
pygame.mixer.music.load("output.mp3")
pygame.mixer.music.play()
while pygame.mixer.music.get_busy():
    pygame.time.Clock().tick(10)
