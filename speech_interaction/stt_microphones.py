import speech_recognition as sr

# List all available microphones
microphones = sr.Microphone.list_microphone_names()
for i, mic in enumerate(microphones):
    print(f"Microphone {i}: {mic}")
