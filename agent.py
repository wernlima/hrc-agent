TEXT_INPUT = 1
TTS_WITH_OPENAI = 0
AUDIO_OUTPUT=0
import time
import typing
from gtts import gTTS
from datetime import datetime
from pymongo import MongoClient
import speech_recognition as sr
from langchain_openai import ChatOpenAI
from langchain_core.messages import AIMessage, HumanMessage, ToolMessage
from langchain_core.prompts import ChatPromptTemplate, SystemMessagePromptTemplate, PromptTemplate, MessagesPlaceholder, HumanMessagePromptTemplate
from langchain.agents import AgentExecutor
import langchain_core.messages.ai, langchain_core.messages.human, langchain_core.messages.function, langchain_core.messages.system, langchain_core.messages.tool, langchain_core.messages.chat

from langchain_community.chat_message_histories import ChatMessageHistory
from langchain_core.runnables.history import RunnableWithMessageHistory

from credentials import OpenAI_API_KEY



from functions_sim import (
    pick_place, go_to_ee_pos, start_full_compliancy, stop_full_compliancy, 
    gripper_open, gripper_close, get_names_of_available_objects, get_pose_from_object
)

tools = [gripper_open, gripper_close, go_to_ee_pos, start_full_compliancy, stop_full_compliancy, get_names_of_available_objects, get_pose_from_object]

# Connect to MongoDB
client = MongoClient('localhost', 27017)
db = client['environment_db']

# Get the current date to use in the collection name
formatted_date = datetime.now().strftime("%Y-%m-%d")
collection_name = f"Log_{formatted_date}"
log_collection = db[collection_name]

def log_command_out(command):
    log_entry = {
        "type": "agent_out",
        "timestamp": datetime.now(),
        "command": command
    }
    log_collection.insert_one(log_entry)

def log_command_in(command):
    log_entry = {
        "type": "agent_in",
        "timestamp": datetime.now(),
        "command": command
    }
    log_collection.insert_one(log_entry)

def log_command_sys(command):
    log_entry = {
        "type": "start",
        "timestamp": datetime.now(),
        "command": command
    }
    log_collection.insert_one(log_entry)

log_command_sys("-----------------------\n New conversation started")

def listen():
    # Use the default microphone as the audio source
    with sr.Microphone() as source:
        print("Adjusting for ambient noise... Please wait.")
        # Adjust for ambient noise levels
        r.adjust_for_ambient_noise(source)
        print("Listening...")
        
        try:
            # Listen to the microphone input
            audio_listened = r.listen(source)
            print("Processing...")
            
            ## Start processing time
            start_time = time.time()
            
            # Using the Whisper model for speech recognition
            """text = r.recognize_whisper(
                audio_listened,
                model="large-v3",
                language="english".9*3++     
            )"""
            text = r.recognize_whisper(
                audio_listened,
                model="tiny.en",
                language="english"
            )
            ## End processing time
            end_time = time.time()
            
            
            # Calculate and print the processing time
            processing_time = end_time - start_time
            print("-"*30)
            print(f"Processing time: {processing_time:.2f} seconds")
            print(f"processed text: {text}")
            print("-"*30)
            return text

        except sr.UnknownValueError:
            print("Speech Recognition could not understand audio")
            return None

        except sr.RequestError as e:
            print(f"Could not request results from Speech Recognition service; {e}")
            return None

        except Exception as e:
            print(f"Error: {e}")
            return None

message_history = ChatMessageHistory()
examples = [


    ################################################
    ################## Get the Knife ###############
    ################################################

    HumanMessage(
        "I would like to cut my apple into smaller pieces.", name="example_user"
    ),
    AIMessage(
        "",
        name="example_assistant",
        tool_calls=[
            {"name": "get_names_of_available_objects", "args": {}, "type": "tool_call", "id": "1"}
        ],
    ),
    ToolMessage("['knife', 'spoon', 'fork', 'plate', 'cup']", tool_call_id="1"),
    AIMessage(
        "",
        name="example_assistant",
        tool_calls=[
            {"name": "get_pose_from_object", "args": {'name': 'knife'}, "type": "tool_call", "id": "2"}
        ],
    ),
    ToolMessage("[0.5, 0.2, 0.15380923759690641]", tool_call_id="2"),
    AIMessage(
        "",
        name="example_assistant",
        tool_calls=[{"name": "gripper_open", "args": {}, "type": "tool_call", "id": "3"}],
    ),
    ToolMessage("True", tool_call_id="3"),
    AIMessage(
        "",
        name="example_assistant",
        tool_calls=[{"name": "go_to_ee_pos", "args": {'position': [0.5, 0.2, 0.15], 'orientation': [0, 0, 1, 0]}, "type": "tool_call", "id": "4"}],
    ),
    ToolMessage("We have reached the target [0.5, 0.2, 0.15]!", tool_call_id="4"),
    AIMessage(
        "",
        name="example_assistant",
        tool_calls=[{"name": "gripper_close", "args": {}, "type": "tool_call", "id": "5"}],
    ),
    ToolMessage("True", tool_call_id="5"),
    AIMessage(
        "",
        name="example_assistant",
        tool_calls=[{"name": "go_to_ee_pos", "args": {'position': [0.3, 0.0, 0.5], 'orientation': [0, 0, 1, 0]}, "type": "tool_call", "id": "6"}],
    ),
    ToolMessage("We have reached the target [0.3, 0.0, 0.5]!", tool_call_id="6"),
    AIMessage( "I brought you a knife to cut your apple", name="example_assistant" ),
    
    ]


message_history.add_messages(examples)


system_prompt = "You are a very powerful assistant, controlling a robot arm.You are called Vinci. go_to_ee_pos has the format for the home position of ('position': [0.3, -0.0, 0.5], 'orientation': [0, 0, 1, 0]). With the robotic arm, you can interact with the available objects, also you have a history. Make sure z is always larger than 0.1, but also low enough to facilitate successful interactions with objects. Only change the z-Coordinate if it is strictly below 0.1. Make sure you execute the functions as shown in the examples"

prompt=ChatPromptTemplate(input_variables=['agent_scratchpad', 'input'],
                        optional_variables=['chat_history'],
                        input_types={'chat_history': typing.List[typing.Union[langchain_core.messages.ai.AIMessage, langchain_core.messages.human.HumanMessage, langchain_core.messages.chat.ChatMessage, langchain_core.messages.system.SystemMessage, langchain_core.messages.function.FunctionMessage, langchain_core.messages.tool.ToolMessage]], 'agent_scratchpad': typing.List[typing.Union[langchain_core.messages.ai.AIMessage, langchain_core.messages.human.HumanMessage, langchain_core.messages.chat.ChatMessage, langchain_core.messages.system.SystemMessage, langchain_core.messages.function.FunctionMessage, langchain_core.messages.tool.ToolMessage]]},
                        partial_variables={'chat_history': [message_history]},
                        metadata={},
                        messages=[SystemMessagePromptTemplate(prompt=PromptTemplate(input_variables=[], template=system_prompt)), MessagesPlaceholder(variable_name='chat_history', optional=True), HumanMessagePromptTemplate(prompt=PromptTemplate(input_variables=['input'], template='{input}')), MessagesPlaceholder(variable_name='agent_scratchpad')])


from langchain.agents import create_tool_calling_agent

agent = create_tool_calling_agent(ChatOpenAI(model="gpt-4o-mini", temperature=0.01, openai_api_key=OpenAI_API_KEY), tools, prompt)

from langchain.agents import AgentExecutor

agent_executor = AgentExecutor(agent=agent, tools=tools, verbose=True, return_intermediate_steps=True)


agent_with_chat_history = RunnableWithMessageHistory(
    agent_executor,
    # This is needed because in most real world scenarios, a session id is needed
    # It isn't really used here because we are using a simple in memory ChatMessageHistory
    lambda session_id: message_history,
    input_messages_key="input",
    history_messages_key="chat_history",
)


while True:
    if TEXT_INPUT:
        user_input = input("Enter your command: ")
    else:
        r = sr.Recognizer()
        user_input = listen()

    if user_input.lower() in ["exit", "quit", "end", "fuck off", "exit.", "quit.", "end."]:
        print("Exiting the interaction.")
        break
    # Log and add user input to history

    log_command_in(f"User Command: {user_input}")
    
    # Get the response from the agent
    response = agent_with_chat_history.invoke({"input": user_input}, config={"configurable": {"session_id": "best_session"}})
    response_text = response['output']

    # Log the command executed by the agent
    log_command_out(f"Agent Response: {response_text}")

    # Print response
    print("-" * 30)
    print(response_text)
    if AUDIO_OUTPUT or TTS_WITH_OPENAI:
        if TTS_WITH_OPENAI:
            from openai import OpenAI
            import pygame
            from credentials import OpenAI_API_KEY
            client = OpenAI(api_key=OpenAI_API_KEY)

            response = client.audio.speech.create(
                model="tts-1",
                voice="shimmer",
                input=response_text,
            )

            response.write_to_file("output.mp3")

            pygame.mixer.init()
            pygame.mixer.music.load("output.mp3")
            pygame.mixer.music.play()
            while pygame.mixer.music.get_busy():
                pygame.time.Clock().tick(10)

        else:
        # Text to speech
            tts = gTTS(text=response_text, lang='en')
            tts.save("output.mp3")
            pygame.mixer.music.load("output.mp3")
            pygame.mixer.music.play()
            while pygame.mixer.music.get_busy():
                pygame.time.Clock().tick(10)
            print("-" * 30)


while True:
    input_text = input("Enter your query: ")
    if input_text.lower() == "exit":
        print("Exiting...")
        break
    response = agent_with_chat_history.invoke({"input": input_text}, config={"configurable": {"session_id": "best_session"}})
    print("-"*30)
    print(response['output'])
    print("-"*30)

