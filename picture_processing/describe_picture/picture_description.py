import os
import time
from pymongo import MongoClient
from PIL import Image as PILImage, ImageDraw, ImageFont
import textwrap
import base64
import requests
from credentials import OpenAI_API_KEY


# Function to encode the image to base64
def encode_image(image_path):
    with open(image_path, "rb") as image_file:
        return base64.b64encode(image_file.read()).decode('utf-8')

# Function to send a request to OpenAI with the base64 encoded image
def generate_and_save_image(api_key, prompt_text, file_name, image_number):
    output_folder = "picture_processing/describe_picture"
    output_path = os.path.join(output_folder, f"{file_name}_{image_number}.jpg")

    # Check if the processed image already exists
    if os.path.exists(output_path):
        print(f"Processed image {output_path} already exists.")
        return
    else:
        print(f"I will ask gpt-4o")
        image_path = f"picture_processing/describe_picture/{image_number}.jpg"
        if not os.path.exists(image_path):
            print(f"Image file {image_path} not found.")
            return

        base64_image = encode_image(image_path)

        headers = {
            "Content-Type": "application/json",
            "Authorization": f"Bearer {api_key}"
        }

        payload = {
            "model": "gpt-4o",
            "messages": [
                {
                    "role": "user",
                    "content": [
                        {
                            "type": "text",
                            "text": prompt_text
                        },
                        {
          "type": "image_url",
          "image_url": {
            "url": f"data:image/jpeg;base64,{base64_image}"
                        }
                        }
                    ]
                }
            ],
            "max_tokens": 300
        }

        response = requests.post("https://api.openai.com/v1/chat/completions", headers=headers, json=payload).json()
        print(response)
        response_text = response['choices'][0]['message']['content']

        original_image = PILImage.open(image_path)
        small_image = original_image.resize((int(original_image.width * 0.1), int(original_image.height * 0.1)))

        new_image = PILImage.new("RGB", (small_image.width * 5, small_image.height + 1000), "white")
        new_image.paste(small_image, (0, 0))

        draw = ImageDraw.Draw(new_image)
        font_path = "picture_processing/Raleway-VariableFont_wght.ttf"
        font = ImageFont.truetype(font_path, 32)
        wrapped_prompt = textwrap.fill(prompt_text, width=100)
        wrapped_response = textwrap.fill(response_text, width=100)

        # Calculate text background box
        prompt_text_size = draw.textsize(wrapped_prompt, font=font)
        response_text_size = draw.textsize(wrapped_response, font=font)
        max_text_width = max(prompt_text_size[0], response_text_size[0])

        # Define positions
        text_position = (10, small_image.height + 10)
        background_position = [
            text_position[0] - 5, text_position[1] - 5,
            text_position[0] + max_text_width + 10, 
            text_position[1] + prompt_text_size[1] + response_text_size[1] + 50
        ]

        # Define the colors
        white_color = (255, 255, 255)
        dark_red = (139, 0, 0)

        # Draw the white background rectangle
        draw.rectangle(background_position, fill=white_color)

        # Draw the text onto the image in dark red on top of the white background
        draw.text(text_position, f"Prompt:\n{wrapped_prompt}\n\nResponse:\n{wrapped_response}\n\n", fill=dark_red, font=font)

        os.makedirs(output_folder, exist_ok=True)
        new_image.save(output_path)

prompt_text = "Describe the following picture. What is beeing shown. What could have happened before, what happens afterwards? Whot objects are present?"

for i in range(0, 7):  # Adjust the range as needed
    generate_and_save_image(OpenAI_API_KEY, prompt_text, "output_gpt_4o", i)
    time.sleep(0)
