import os
import time
from pymongo import MongoClient
from PIL import Image as PILImage, ImageDraw, ImageFont
import textwrap
import base64
import requests
from credentials import OpenAI_API_KEY
import json

# Function to fetch data from MongoDB
def fetch_data():
    client = MongoClient('localhost', 27017)
    db = client['environment_db']
    return (
        db['overall_task'].find_one()["overall_task"],
        [obj['name'] for obj in db['objects'].find()],
        list(db['workflow'].find())
    )

# Function to encode the image to base64
def encode_image(image_path):
    with open(image_path, "rb") as image_file:
        return base64.b64encode(image_file.read()).decode('utf-8')

def parse_input_json(input_json):
    try:
        # Remove markdown code block indicators if present
        clean_json = input_json.strip('` ').replace("json","")
        # Parse the cleaned JSON string
        data = json.loads(clean_json)
        
        # Extract values into variables
        action = data.get("action", "")
        problem = data.get("problem", False)
        following_workflow = data.get("following_workflow", False)
        phase_task = data.get("Phase_Task", [])
        phase_task_second_guess = data.get("Phase_Task_second_guess", [])
        phase_task_third_guess = data.get("Phase_Task_third_guess", [])

        # Return the extracted values
        return action, problem, following_workflow, phase_task, phase_task_second_guess, phase_task_third_guess
    except json.JSONDecodeError:
        # Return None or default values in case of JSON parsing error
        print("Invalid JSON input.")
        return None, None, None, None, None, None


# Function to send a request to OpenAI with the base64 encoded image
def generate_and_save_image(api_key, prompt_text, file_name, image_number):
    output_folder = "Pics/Pics_commented"
    output_path = os.path.join(output_folder, f"{file_name}_{image_number}.jpg")
    json_output_path = os.path.join(output_folder, f"{file_name}_{image_number}.json")

    # Check if the processed image already exists
    if os.path.exists(output_path):
        print(f"Processed image {output_path} already exists.")
        # Load and return the response from the JSON file if it exists
        if os.path.exists(json_output_path):
            with open(json_output_path, 'r') as json_file:
                return json.load(json_file)
        else:
            print(f"JSON file {json_output_path} not found.")
            return None
    else:
        image_path = f"Pics/{image_number}.jpg"
        if not os.path.exists(image_path):
            print(f"Image file {image_path} not found.")
            return None

        base64_image = encode_image(image_path)

        headers = {
            "Content-Type": "application/json",
            "Authorization": f"Bearer {api_key}"
        }

        payload = {
            "model": "gpt-4o-2024-05-13",
            "messages": [
                {
                    "role": "user",
                    "content": [
                        {
                            "type": "text",
                            "text": prompt_text
                        },
                        {
                            "type": "image_url",
                            "image_url": {
                                "url": f"data:image/jpeg;base64,{base64_image}"
                            }
                        }
                    ]
                }
            ],
            "max_tokens": 300
        }

        # Timing the request
        start_time = time.time()
        response = requests.post("https://api.openai.com/v1/chat/completions", headers=headers, json=payload).json()
        end_time = time.time()
        execution_time = end_time - start_time

        response_text = response['choices'][0]['message']['content']

        # Save response to JSON file
        with open(json_output_path, 'w') as json_file:
            json.dump(response_text, json_file, indent=2)

        original_image = PILImage.open(image_path)
        small_image = original_image.resize((int(original_image.width * 0.1), int(original_image.height * 0.1)))

        new_image = PILImage.new("RGB", (small_image.width * 5, small_image.height + 1000), "white")
        new_image.paste(small_image, (0, 0))

        draw = ImageDraw.Draw(new_image)
        font_path = "Raleway-VariableFont_wght.ttf"
        font = ImageFont.truetype(font_path, 24)
        prompt_text_short = prompt_text.replace(f"{workflow}", "[workflow]")
        wrapped_prompt = textwrap.fill(prompt_text_short, width=100)
        wrapped_response = textwrap.fill(response_text, width=100)
        draw.text((10, small_image.height + 10), f"Prompt:\n {wrapped_prompt}\n\nResponse:\n{wrapped_response}\n\nExecution Time: {execution_time:.2f} seconds", fill="black", font=font)
        os.makedirs(output_folder, exist_ok=True)
        new_image.save(output_path)

        return response_text

def replace_current_action(client, phase_id, task_id, following_workflow, problem, action):
    db = client['environment_db']
    workflow_collection = db['workflow']
    current_action_collection = db['current_action']

    # Fetch the actor from the workflow collection
    actor = None
    workflow_entry = workflow_collection.find_one({"Phase_ID": phase_id, "Tasks.Task_ID": task_id})
    if workflow_entry:
        # Extract the actor for the specific task
        tasks = workflow_entry.get("Tasks", [])
        for task in tasks:
            if task.get("Task_ID") == task_id:
                actor = task.get("Actor")
                break

    if not actor:
        actor="Human"
        print("No actor found for the given Phase_ID and Task_ID.")

    # Create a new document with the fetched actor and other details
    new_action_details = {
        "Task_ID": task_id,
        "Phase_ID": phase_id,
        "Actor": actor,
        "Description": action,
        "following_workflow": following_workflow,
        "problem": problem
    }

    # Delete all existing documents in the collection
    current_action_collection.delete_many({})

    # Insert the new document into the collection
    result = current_action_collection.insert_one(new_action_details)
    
    # Return the ID of the newly inserted document
    return str(result.inserted_id)

# Your OpenAI API key
api_key = OpenAI_API_KEY

task, objects_list, workflow = fetch_data()
prompt_text = f"{task} is the task being performed.\n To perform the task, the following workflow was generated: {workflow}\n What is the action taking place in the picture? Is there an acute problem requiring immediate intervention? Is an action of the workflow being performed? Consider logical or typical actions that might not be explicitly described in the workflow but are part of the cooking process. If we follow the workflow and there is no problem, go through all the phases and all the tasks in each phase. Which Phase_ID and which Task_ID most likely correspond to the current state of the task? Also, make a second and a third guess for the current state of the task. If we don't follow the workflow or there is a problem, all your guesses should be [-1, -1]. Respond as short as possible in JSON format ('action': <String>, 'problem': <Bool>, 'following_workflow': <Bool>, 'Phase_Task': [<Phase_ID:int>, <Task_ID:int>]), 'Phase_Task_second_guess': [<Phase_ID:int>, <Task_ID:int>]), 'Phase_Task_third_guess': [<Phase_ID:int>, <Task_ID:int>])"

for i in range(0,7):
    response = generate_and_save_image(api_key, prompt_text, "output_gpt_4o", i)
    action, problem, following_workflow, phase_task, phase_task_second_guess, phase_task_third_guess = parse_input_json(response)
    print("Action:", action)
    print("Problem:", problem)
    print("Following Workflow:", following_workflow)
    print("Phase/Task:", phase_task)
    replace_current_action(client = MongoClient('localhost', 27017), action =action, problem=problem, following_workflow=following_workflow, phase_id=phase_task[0], task_id=phase_task[1])
