import os
import time
from pymongo import MongoClient
from PIL import Image as PILImage, ImageDraw, ImageFont
import textwrap
import base64
import requests
from credentials import OpenAI_API_KEY

# Function to fetch data from MongoDB
def fetch_data():
    client = MongoClient('localhost', 27017)
    db = client['environment_db']
    return (
        db['overall_task'].find_one()["overall_task"],
        [obj['name'] for obj in db['objects'].find()],
        list(db['workflow'].find())
    )

# Function to encode the image to base64
def encode_image(image_path):
    with open(image_path, "rb") as image_file:
        return base64.b64encode(image_file.read()).decode('utf-8')

# Function to send a request to OpenAI with the base64 encoded image
def generate_and_save_image(api_key, prompt_text, file_name, image_number):
    output_folder = "Pics/Pics_commented"
    output_path = os.path.join(output_folder, f"{file_name}_{image_number}.jpg")

    # Check if the processed image already exists
    if os.path.exists(output_path):
        print(f"Processed image {output_path} already exists.")
        return
    else:
        print(f"I will ask gpt-4o")
        image_path = f"Pics/{image_number}.jpg"
        if not os.path.exists(image_path):
            print(f"Image file {image_path} not found.")
            return

        base64_image = encode_image(image_path)

        headers = {
            "Content-Type": "application/json",
            "Authorization": f"Bearer {api_key}"
        }

        payload = {
            "model": "gpt-4o",
            "messages": [
                {
                    "role": "user",
                    "content": [
                        {
                            "type": "text",
                            "text": prompt_text
                        },
                        {
          "type": "image_url",
          "image_url": {
            "url": f"data:image/jpeg;base64,{base64_image}"
                        }
                        }
                    ]
                }
            ],
            "max_tokens": 300
        }

        response = requests.post("https://api.openai.com/v1/chat/completions", headers=headers, json=payload).json()
        print(response)
        response_text = response['choices'][0]['message']['content']

        original_image = PILImage.open(image_path)
        small_image = original_image.resize((int(original_image.width * 0.1), int(original_image.height * 0.1)))

        new_image = PILImage.new("RGB", (small_image.width * 5, small_image.height + 1000), "white")
        new_image.paste(small_image, (0, 0))

        draw = ImageDraw.Draw(new_image)
        font_path = "Raleway-VariableFont_wght.ttf"
        font = ImageFont.truetype(font_path, 24)
        prompt_text_short = prompt_text.replace(f"{workflow}", "[workflow]")
        wrapped_prompt = textwrap.fill(prompt_text_short, width=100)
        wrapped_response = textwrap.fill(response_text, width=100)
        draw.text((10, small_image.height + 10), f"Prompt:\n {wrapped_prompt}\n\nResponse:\n{wrapped_response}\n\n", fill="black", font=font)
        os.makedirs(output_folder, exist_ok=True)
        new_image.save(output_path)

# Your OpenAI API key
api_key = OpenAI_API_KEY

task, objects_list, workflow = fetch_data()
prompt_text = f"{task} is the task being performed.\n To perform the task, the following workflow was generated: {workflow}\n What is the action taking place in the picture? Is there an acute problem requiring immediate intervention? Is an action of the workflow being performed? Consider logical or typical actions that might not be explicitly described in the workflow but are part of the cooking process. If we follow the workflow and there is no problem, go through all the phases and all the tasks in each phase. Which Phase_ID and which Task_ID most likely correspond to the current state of the task? Also, make a second and a third guess for the current state of the task. If we don't follow the workflow or there is a problem, all your guesses should be [-1, -1]. Respond as short as possible in JSON format ('action': <String>, 'problem': <Bool>, 'following_workflow': <Bool>, 'Phase_Task': [<Phase_ID:int>, <Task_ID:int>]), 'Phase_Task_second_guess': [<Phase_ID:int>, <Task_ID:int>]), 'Phase_Task_third_guess': [<Phase_ID:int>, <Task_ID:int>])"

for i in range(0, 7):  # Adjust the range as needed
    generate_and_save_image(api_key, prompt_text, "output_gpt_4o", i)
    time.sleep(0)
