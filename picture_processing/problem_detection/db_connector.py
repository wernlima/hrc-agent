from pymongo import MongoClient

def fetch_data():
    client = MongoClient('localhost', 27017)
    db = client['environment_db']
    return (
        db['overall_task'].find_one()["overall_task"],
        [obj['name'] for obj in db['objects'].find()],
        list(db['workflow'].find())
    )

task, objects_list, workflow = fetch_data()

print(f"{'-'*30}\nTask: {task}\n{'-'*30}\nObjects: {objects_list}\n{'-'*30}\nWorkflow: {workflow}\n{'-'*30}")
